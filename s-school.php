<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Lifetouch</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' href='assets/css/s-style.css'>
    <!-- <script src="assets/js/main.js" defer></script> -->
</head>

<body>

<div class="main-container">

    <div class="page-title">
        <h3>  We're here to help! </h3>
        <p>  Support for Schools and Yearbook Advisers </p>
        <a class="text-link">Parent and Individual Support </a> 
    </div>

    <div class="main-form">
        <?php
        include_once 's-form.php';
        ?>
    </div>

</div>

</body>
</html>
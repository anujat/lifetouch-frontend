<?php
include_once 'header.php';
?>

<ul class="tabs">
    <li class="individual-tab active">
        <a> I am an individual </a>
    </li>
    <li class="school-tab">
        <a>I am a school or organization</a>
    </li>
</ul>

<div class="individual-tab-block">
    <div class="content">
        <div class="tab-section">
            <div class="col col-span-4">
                    <div class="block">
                        <img src="assets/images/1.svg" />
                        <div class="info-block">
                            <h1>Preschool Photography</h1>
                            <div class="sub-info">
                                <div class="flex">
                                    <p> <img src="assets/images/question-circle.svg" /><a href="#">Frequently Asked Questions</a></p>
                                    <p> <img src="assets/images/headset.svg" /><a href="#">Live Help with Customer Service</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col col-span-4">
                    <div class="block">
                        <img src="assets/images/2.svg" />
                        <div class="info-block">
                            <h1>K-12/Underclass Photography</h1>
                            <div class="sub-info">
                                <div class="flex">
                                    
                                    <p><img src="assets/images/question-circle.svg" /><a href="#">Frequently Asked Questions</a></p>
                                    <p><img src="assets/images/camera.svg" /><a href="#">Find My Picture Day</a></p>
                                    <p><img src="assets/images/headset.svg" /><a href="#">Live Help with Customer Service</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col col-span-4">
                <div class="block">
                    <img src="assets/images/3.svg" />
                    <div class="info-block">
                        <h1>Senior Photography</h1>
                        <div class="sub-info">
                            <div class="flex">
                                
                                <p><img src="assets/images/question-circle.svg" /><a href="#">Frequently Asked Questions</a></p>
                                <p><img src="assets/images/calendar-alt.svg" /><a href="#">Schedule My Appointment</a></p>
                                <p><img src="assets/images/clipboard-list-check.svg" /><a href="#">How to Prep for My Session</a></p>
                                <p><img src="assets/images/map-marker-check.svg" /><a href="#">Find a Studio Location</a></p>
                                <p><img src="assets/images/headset.svg" /><a href="#">Live Help with Customer Service</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-span-4">
                <div class="block">
                    <img src="assets/images/4.svg" />
                    <div class="info-block">
                        <h1>Commencements Photography</h1>
                        <div class="sub-info">
                            <div class="flex">
                                
                                <p><img src="assets/images/question-circle.svg" /><a href="#">Frequently Asked Questions</a></p>
                                <p><img src="assets/images/headset.svg" /><a href="#">Live Help with Customer Service</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-span-4">
                <div class="block">
                    <img src="assets/images/5.svg" />
                    <div class="info-block">
                        <h1>Special Events Photography</h1>
                        <div class="sub-info">
                            <div class="flex">
                                
                                <p><img src="assets/images/question-circle.svg" /><a href="#">Frequently Asked Questions</a></p>
                                <p><img src="assets/images/headset.svg" /><a href="#">Live Help with Customer Service</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-span-4">
                <div class="block">
                    <img src="assets/images/6.svg" />
                    <div class="info-block">
                        <h1>Sports Photography</h1>
                        <div class="sub-info">
                            <div class="flex">
                                
                                <p><img src="assets/images/question-circle.svg" /><a href="#">Frequently Asked Questions</a></p>
                                <p><img src="assets/images/headset.svg" /><a href="#">Live Help with Customer Service</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-span-4">
                <div class="block">
                    <img src="assets/images/7.svg" />
                    <div class="info-block">
                        <h1>JCPenney Portrait Photography</h1>
                        <div class="sub-info">
                            <div class="flex">
                                
                                <p><img src="assets/images/question-circle.svg" /><a href="#">Frequently Asked Questions</a></p>
                                <p><img src="assets/images/map-marker-check.svg" /><a href="#">Find a Studio Location</a></p>
                                <p><img src="assets/images/camera.svg" /><a href="#">Business Headshots</a></p>
                                <p><img src="assets/images/headset.svg" /><a href="#">Live Help with Customer Service</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-span-4">
                <div class="block">
                    <img src="assets/images/8.svg" />
                    <div class="info-block">
                        <h1>Yearbook Orders</h1>
                        <div class="sub-info">
                            <div class="flex">
                                
                                <p><img src="assets/images/question-circle.svg" /><a href="#">Frequently Asked Questions</a></p>
                                <p><img src="assets/images/headset.svg" /><a href="#">Live Help with Customer Service</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
           
        </div>
    </div>
</div>
<div class="school-tab-block">
    <div class="content">
        <div class="tab-section">
            <div class="col col-span-4">
                    <div class="block">
                        <img src="assets/images/organization-1.svg" />
                        <div class="info-block">
                            <h1>Preschool Photography</h1>
                            <div class="sub-info">
                                <div class="flex">
                                    <p><img src="assets/images/info-square.svg" /><a href="#">I want program information</a></p>
                                    <p><img src="assets/images/comment-alt-edit.svg" /><a href="#">I’d like someone to contact me</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col col-span-4">
                    <div class="block">
                        <img src="assets/images/organization-2.svg" />
                        <div class="info-block">
                            <h1>K-12/Underclass Photography</h1>
                            <div class="sub-info">
                                <div class="flex">
                                    <p><img src="assets/images/info-square.svg" /><a href="#">I want program information</a></p>
                                    <p><img src="assets/images/comment-alt-edit.svg" /><a href="#">I’d like someone to contact me</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col col-span-4">
                <div class="block">
                    <img src="assets/images/organization-3.svg" />
                    <div class="info-block">
                        <h1>Senior Photography</h1>
                        <div class="sub-info">
                            <div class="flex">
                                <p><img src="assets/images/info-square.svg" /><a href="#">I want program information</a></p>
                                <p><img src="assets/images/map-marker-check.svg" /><a href="#">Find a Studio Location</a></p>
                                <p><img src="assets/images/comment-alt-edit.svg" /><a href="#">I’d like someone to contact me</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-span-4">
                <div class="block">
                    <img src="assets/images/organization-4.svg" />
                    <div class="info-block">
                        <h1>Business Photography</h1>
                        <div class="sub-info">
                            <div class="flex">
                                <p><img src="assets/images/info-square.svg" /><a href="#">I want program information</a></p>
                                <p><img src="assets/images/comment-alt-edit.svg" /><a href="#">I’d like someone to contact me</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-span-4">
                <div class="block">
                    <img src="assets/images/organization-5.svg" />
                    <div class="info-block">
                        <h1>Yearbooks Programs</h1>
                        <div class="sub-info">
                            <div class="flex">
                                <p><img src="assets/images/info-square.svg" /><a href="#">I want program information</a></p>
                                <p><img src="assets/images/comment-alt-edit.svg" /><a href="#">I’d like someone to contact me</a></p>
                                <p><img src="assets/images/concierge-bell.svg" /><a href="#">Adviser Help & Support</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

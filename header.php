<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Lifetouch</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' href='assets/css/style.css'>
    <script src="assets/js/main.js" defer></script>
</head>

<body>

    <div class="sample-page">
        <div class="main-container">
            <div class="page-title">
                <h3>We're here to help!</h3>
                <p>Due to Covid, we are experiencing longer than normal wait times for customer service.
                    We apologize for the inconvenience</p>

            </div>
            <div class="page-content">
                <div>
                    <?php
                        include_once 'nav.php';
                    ?>
                </div>
                <div class="main-block">
                    

                </div>

            </div>

        </div>
    </div>
   
</body>
</html>

